// code for prepping the initial view of the page

const key = '186d1e624c181429eaed0136dd73c4ca'; // trello API Key
const token =
  '7a183e73b17326f5fc609374ab16b8df0e396656dc00677e2d92baa9f2c3acf9';
const listID = '5e0590b725fc444f57737d10';

/**
 * fetches the cards from the trello API, formats it to JSON and passes
 * it to the addCardsToDOM() to add them to the DOM
 */
const prepList = () => {
  fetch(
    `https://api.trello.com/1/lists/${listID}/cards?fields=name&key=${key}&token=${token}`
  )
    .then(response => {
      return response.json();
    })
    .then(response => {
      addCardsToDOM(response);
    })
    .catch(err => {
      console.log(err);
    });
};
window.onload = prepList();

const modal = document.querySelector('#myModal');
const instance = M.Modal.init(modal);
const list = document.querySelector('#list1');
list.addEventListener('click', e => {
  const eventClass = e.target.className;
  const eventId = e.target.id;
  if (eventClass == 'card') {
    getCardDetails(eventId)
      .then(card => {
        modal.setAttribute('data-cardId', eventId);
        // set the modal header with card title
        document.querySelector('#cardHeader').textContent = card.name;
        displayCheckLists(card.idChecklists);
        // reset the card content container
        document.querySelector('#cardContentContainer').innerHTML = '';
        // register delete button to 'clicked' card and pass modal instance
        // registerDeleteCardBtn(eventId, instance);
      })
      .then(() => {
        //   open modal only after prepping is done
        instance.open();
      });
  }
});

modal.addEventListener('click', e => {
  const cardId = modal.getAttribute('data-cardId');
  if (e.target.id == 'deleteCardBtn') {
    deleteCard(cardId);
  } else if (e.target.id == 'addAnotherCheckListBtn') {
    const addCheckListOptions = document.getElementById('addCheckListOptions');
    addCheckListOptions.style.display = 'block';
  } else if (e.target.id == 'addCheckList') {
    addCheckListEventListener(cardId);
  } else if (e.target.id == 'cancelAddCheckListBtn') {
    const addCheckListOptions = document.getElementById('addCheckListOptions');
    cancelAddCheckListEventListener(addCheckListOptions);
  }
});

// add event listener for the addAnotherCard btn
const addAnotherCardBtn = document.querySelector(
  '#list1 > .addAnotherCard-btn'
);
addAnotherCardBtn.addEventListener('click', () => {
  // make it disappear
  addAnotherCardBtn.style.display = 'none';
  displayAddCardOptions('block');
});
const addCardOptions = document.querySelector('#list1 > .addCardOptions');
const displayAddCardOptions = display => {
  addCardOptions.style.display = display;
};
// add event listener for the cancelAddCard btn
const cancelAddCardBtn = document.querySelector('#cancelAddCard-btn');
cancelAddCardBtn.addEventListener('click', () => {
  displayAddCardOptions('none');
  addAnotherCardBtn.style.display = 'block';
});

// adding card and other functionalities
const addCardBtn = document.querySelector('#addCard-btn');
addCardBtn.addEventListener('click', () => {
  const cardTitleTextArea = document.querySelector('#cardTitle-txtArea');
  const cardTitle = cardTitleTextArea.value;
  if (cardTitle) {
    addCardToTrello([cardTitle]);
    // empty the text area
    cardTitleTextArea.value = '';
  }
});
const addCardToTrello = cardTitle => {
  const URL = `https://api.trello.com/1/cards?name=${cardTitle}&idList=${listID}&keepFromSource=all&key=${key}&token=${token}`;
  fetch(URL, {
    method: 'POST'
  })
    .then(response => {
      return response.json();
    })
    .then(response => {
      addCardsToDOM([{ id: response.id, name: response.name }]);
    })
    .catch(err => {
      console.log(err);
    });
};
const addCardsToDOM = cardList => {
  cardList.forEach(card => {
    const cardDiv = document.createElement('div');
    // give class name for styling
    cardDiv.className = 'card';
    // set data target to modal ID to link it with the modal
    cardDiv.setAttribute('dataTarget', '#modal1');
    // set id of card fetched for code modularity
    cardDiv.id = card.id;
    // create a text node with card title as text
    const titleText = document.createTextNode(card.name);
    // append it to the card div
    cardDiv.appendChild(titleText);
    // insert before the add another card button
    list.insertBefore(cardDiv, addAnotherCardBtn);
  });
};

const getCardDetails = cardId => {
  const URL = `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`;
  return fetch(URL).then(response => response.json());
};

const deleteCard = cardId => {
  const URL = `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`;
  fetch(URL, {
    method: 'DELETE'
  }).then(() => {
    if (document.getElementById(cardId)) {
      // target node to be deleted
      const node = document.getElementById(cardId);
      // get parent then remove child node
      node.parentNode.removeChild(node);
      // close modal after card deletetion
      instance.close();
    }
  });
};

const addCheckListEventListener = cardId => {
  const checkListTitleTextArea = document.getElementById('addCheckListTitle');
  const checkListTitle = checkListTitleTextArea.value;
  if (checkListTitle) {
    const URL = `https://api.trello.com/1/checklists?idCard=${cardId}&name=${checkListTitle}&key=${key}&token=${token}`;
    fetch(URL, {
      method: 'POST'
    })
      .then(response => {
        return response.json();
      })
      .then(checkList => {
        addCheckListToDOM(checkList);
        checkListTitleTextArea.value = '';
      });
  }
};
const cancelAddCheckListEventListener = addCheckListOptions => {
  addCheckListOptions.style.display = 'none';
};
/******************************************************************/

const registerAddCheckItemBtn = (cardId, checkListId) => {
  const checkList = document.getElementById(checkListId);
  const addAnotherCheckItemBtn = checkList.querySelector(
    '.addAnotherCheckItemBtn'
  );
  addAnotherCheckItemBtn.addEventListener('click', () => {
    const addCheckItemOptions = checkList.querySelector('.addCheckItemOptions');
    addCheckItemOptions.style.display = 'block';
    addAnotherCheckItemBtn.style.display = 'none';
    addCheckItemEventListener(cardId, checkList);
    cancelAddCheckItemEventListener(
      checkList,
      addCheckItemOptions,
      addAnotherCheckItemBtn
    );
  });
};
const addCheckItemEventListener = (cardId, checkList) => {
  const checkItemTitleTextArea = checkList.querySelector('.addCheckItemTitle');
  const addCheckItemBtn = checkList.querySelector('.addCheckItem');
  addCheckItemBtn.addEventListener('click', () => {
    const checkItemTitle = checkItemTitleTextArea.value;
    if (checkItemTitle) {
      const URL = `https://api.trello.com/1/checklists/${checkList.id}/checkItems?name=${checkItemTitle}&pos=bottom&checked=false&key=${key}&token=${token}`;
      fetch(URL, {
        method: 'POST'
      })
        .then(response => {
          return response.json();
        })
        .then(checkItem => {
          addCheckItemsToDOM(cardId, checkList.id, [checkItem]);
          checkItemTitleTextArea.value = '';
        });
    }
  });
};
const cancelAddCheckItemEventListener = (
  checkList,
  addCheckItemOptions,
  addAnotherCheckItemBtn
) => {
  const cancelAddCheckItemBtn = checkList.querySelector(
    '.cancelAddCheckItemBtn'
  );
  cancelAddCheckItemBtn.addEventListener('click', () => {
    addCheckItemOptions.style.display = 'none';
    addAnotherCheckItemBtn.style.display = 'block';
  });
};

/******************************************************************* */
const displayCheckLists = idChecklists => {
  if (idChecklists.length !== 0) {
    idChecklists.forEach(id => {
      getCheckList(id).then(checkList => {
        addCheckListToDOM(checkList);
        addCheckItemsToDOM(
          checkList.idCard,
          checkList.id,
          checkList.checkItems
        );
        registerAddCheckItemBtn(checkList.idCard, checkList.id);
      });
    });
  }
};
const getCheckList = id => {
  const URL = `https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`;
  return fetch(URL).then(response => {
    return response.json();
  });
};
const addCheckListToDOM = checkList => {
  const checkListContainer = document.createElement('div');
  checkListContainer.className = 'checkList-container';
  checkListContainer.id = checkList.id;
  checkListContainer.innerHTML = `<div class="checkList-header">
  <div>
    <i class="material-icons">done_all</i>
<span class="checkListTitle">${checkList.name}</span>
  </div>
  <button class="checkList-deleteBtn myBtn cardBtn">Delete</button>
</div><button class="addAnotherCheckItemBtn myBtn cardBtn">
Add an item
</button>
<div class="addCheckItemOptions">
<textarea
  class="addCheckItemTitle"
  placeholder="Add an item"
></textarea>
<input
  type="button"
  value="Add"
  class="addCheckItem myBtn addSomethingBtn"
/>
<input
  type="button"
  value="X"
  class="cancelAddCheckItemBtn myBtn cancelBtn"
/>
</div>`;
  document
    .querySelector('#cardContentContainer')
    .appendChild(checkListContainer);
  deleteCheckListEventListener(checkList.id);
};
let count = 0;
const deleteCheckListEventListener = checkListId => {
  const checkList = document.getElementById(checkListId);
  const deleteBtn = checkList.firstChild.querySelector('button');
  deleteBtn.addEventListener('click', () => {
    const URL = `https://api.trello.com/1/checklists/${checkListId}?key=${key}&token=${token}`;
    fetch(URL, {
      method: 'DELETE'
    }).then(() => {
      checkList.parentNode.removeChild(checkList);
    });
    count++;
  });
  console.log(count);
};

const addCheckItemsToDOM = (cardId, checkListId, checkItems) => {
  checkItems.forEach(item => {
    const checkItem = document.createElement('p');
    checkItem.id = item.id;
    checkItem.className = 'checkItem';
    let state = '';
    if (item.state == 'complete') {
      state = 'checked = "checked"';
    }
    checkItem.innerHTML = `<label>
    <input type="checkbox" class="filled-in" ${state}/>
    <span>${item.name}</span>
  </label>
  <i class="material-icons checklistItem-deleteBtn myBtn">delete</i>`;
    const thisCheckList = document.getElementById(checkListId);
    thisCheckList.insertBefore(
      checkItem,
      thisCheckList.querySelector('.addAnotherCheckItemBtn')
    );
    addCheckItemChangeEventListener(cardId, checkItem.id);
    deleteCheckItemEventListener(cardId, checkItem.id);
  });
};
const addCheckItemChangeEventListener = (cardId, checkItemId) => {
  const checkBox = document.getElementById(checkItemId).firstChild.firstChild
    .nextSibling;
  checkBox.addEventListener('change', () => {
    let state;
    if (checkBox.getAttribute('checked')) {
      state = 'incomplete';
      checkBox.removeAttribute('checked');
    } else {
      checkBox.setAttribute('checked', 'checked');
      state = 'complete';
    }
    const URL = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${state}&key=${key}&token=${token}`;
    fetch(URL, {
      method: 'PUT'
    });
  });
};
const deleteCheckItemEventListener = (cardId, checkItemId) => {
  const checkItem = document.getElementById(checkItemId);
  const deleteBtn = checkItem.lastChild;
  deleteBtn.addEventListener('click', () => {
    const URL = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${key}&token=${token}`;
    fetch(URL, {
      method: 'DELETE'
    }).then(() => {
      checkItem.parentNode.removeChild(checkItem);
    });
  });
};
